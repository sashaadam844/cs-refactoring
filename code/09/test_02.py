import unittest
from unittest.mock import patch
from io import StringIO

from ref_task_02 import main

class TestMainFunction(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["2", "3"])
    def test_main_valid_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)

        output = mock_stdout.getvalue().strip()
        self.assertTrue("Z1 =" in output)
        self.assertTrue("Z2 =" in output)
        self.assertTrue("Z3 =" in output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["abc", "."])
    def test_main_non_numeric_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Некоректні дані. Введіть числові значення для x та y."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["2", "0"])
    def test_main_zero_denominator(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Некоректні дані. Знаменник не може дорівнювати нулю."
        self.assertEqual(output, expected_output)

if __name__ == '__main__':
    unittest.main()
