import math
import sys

def input_float(prompt):
    try:
        return float(input(prompt))
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)

def calculate_z1(x, y):
    d = math.cos(2 * y) - 1
    if d != 0:
        return (math.cos(x) ** 2 + 1) / d
    else:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)

def calculate_z2(x, y):
    return math.atan(math.tan(y / 2 - x / 3))

def calculate_z3(x, y, z1, z2):
    if (math.pi * y) != 0:
        return abs(z2 / z1 - 5 / z1) / (math.pi * y)
    else:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
    sys.exit(1)

def display_result(z1, z2, z3):
    print(f"Z1 = {z1}")
    print(f"Z2 = {z2}")
    print(f"Z3 = {z3}")

def main():
    x = input_float("Введіть значення x: ")
    y = input_float("Введіть значення y: ")

    z1 = calculate_z1(x, y)
    z2 = calculate_z2(x, y)
    z3 = calculate_z3(x, y, z1, z2)

    display_result(z1,z2,z3)

    sys.exit(0)


if __name__ == "__main__":
    main()
