import unittest
from unittest.mock import patch
from io import StringIO
from ref_task_01 import main

class TestTriangleSideLength(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["abc", "."])
    def test_main_non_numeric_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Некоректні дані. Введіть числові значення для периметру та довжин сторін."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["-1", "4", "5"])
    def test_main_negative_perimeter(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Параметри повинні бути додатними числами."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["10", "-4", "5"])
    def test_main_negative_side_a(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Параметри повинні бути додатними числами."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["15", "4", "-5"])
    def test_main_negative_side_b(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Параметри повинні бути додатними числами."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["10", "5", "5"])
    def test_main_triangle_not_exist(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Такий трикутник не існує (неможливо побудувати за заданими сторонами)."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["15", "10", "8"])
    def test_main_valid_triangle(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        output = mock_stdout.getvalue().strip()
        self.assertTrue(output.startswith("Довжина третьої сторони трикутника: "))

if __name__ == '__main__':
    unittest.main()
