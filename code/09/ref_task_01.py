import sys

def input_positive_float(prompt):
    try:
        value = float(input(prompt))
        if value > 0:
            return value
        else:
            print("Параметри повинні бути додатними числами.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для периметру та довжин сторін.")
        sys.exit(1)

def calculate_triangle_side(p, a, b):
    if p >= a + b:
        print("Такий трикутник не існує (неможливо побудувати за заданими сторонами).")
        sys.exit(1)
    else:
        c = p - a - b
        print(f"Довжина третьої сторони трикутника: {c}")
        sys.exit(0)

def display_result(c):
    print(f"Довжина третьої сторони трикутника: {c}")
    sys.exit(0)
def main():
    p = input_positive_float("Введіть периметр трикутника: ")
    a = input_positive_float("Введіть довжину першої сторони: ")
    b = input_positive_float("Введіть довжину другої сторони: ")

    c = calculate_triangle_side(p, a, b)
    display_result(c)


if __name__ == "__main__":
    main()
