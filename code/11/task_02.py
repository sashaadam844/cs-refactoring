import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = ((1 + cos(4 - y)) / (sin(4 - x))) * (cos(2 - x) / (1 + xos( 2 - y + x)))
Z2 = ((x + sin(8 - x)) / cos(x - 4))^1/2
Z3 = (4 - Z1) / (Z2^2 + Z1^3)
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d1 = math.sin(4 - x)
        d2 = 1 + math.cos(2 - y + x)
        if math.sin(4 - x) != 0 and 1 + math.cos(2 - y + x) != 0:
            z1 = ((1 + math.cos(4 - y)) / d1) * (math.cos(2 - x) / d2)
            d3 = math.cos(x - 4)
            if math.cos(x - 4) != 0:
                z2 = math.sqrt((x + math.sin(8 - x)) / d3)
                d4 = (z2 ** 2) + (z1 ** 3)
                if (z2 ** 2) + (z1 ** 3) != 0:
                    z3 = (4 - z1) / d4
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменники не можуть дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
